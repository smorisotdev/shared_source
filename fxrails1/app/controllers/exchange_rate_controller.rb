class ExchangeRateController < ApplicationController
  respond_to :html, :xml, :js
  def index
    ExchangeRate::refresh("./sourceRate.xml")
    @currency = ExchangeRate::get_available_currencies
    ExchangeRate::get_available_dates
  end

  def update
puts "update"
    @date = params[:date]
    @base = params[:currency_base]
    @counter = params[:currency_counter]
    @msg = ExchangeRate::at(@date,@base,@counter)

data = {:message => @msg}
render :json => data, :status => :ok
  end
end
