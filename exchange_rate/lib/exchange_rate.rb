require "exchange_rate/version"
require "rexml/document"
require "rexml/xpath"
require "open-uri"
include REXML

module ExchangeRate

 def self.refresh(source_rate)
  @ex = ExchangeRateCalculator.new
  @ex.refresh(source_rate)
 end

 def self. at(time, base, counter)
   @ex.at(time,base,counter)
 end


 def self. get_available_currencies
   @ex.get_available_currencies
 end

 def self. get_available_dates
   @ex.get_available_dates
 end

 #-----------------------------------------------------

  class ExchangeRateCalculator

    def initialize

    end


  def refresh(source_rate)
    reader = FXReader.new
    @fx_data = reader.xml_to_fx_data(source_rate)

    ref_currency = FXCurrency.new("1","EUR")
    @fx_data.get_data.each do
    |fx_base_date|
      fx_base_date.add(ref_currency)
    end
  end

  def at(time, base, counter)
    fx_base_date_obj = @fx_data.get_data_by_date(time)

    base_currency =  fx_base_date_obj.get_rate_by_currency(base)
    counter_currency =  fx_base_date_obj.get_rate_by_currency(counter)

    rate = counter_currency.get_rate / base_currency.get_rate
  end




  def get_available_currencies
    @fx_data.get_available_currencies
  end

    def get_available_dates
      @fx_data.get_available_dates
    end


  end
#-------------------------- FxReader class
  class FXReader

    def initialize()
    end

    def xml_to_fx_data(xml_file_path)
      f = File.open(xml_file_path)
      xmldoc = Document.new(f)
      data = FXData.new

      REXML::XPath.each(xmldoc,"//Cube[@time]/") do
      |e|
        base = FXBaseDate.new(e.attributes['time'])
        e.each_element do
        |elm|
          cur = FXCurrency.new(elm.attributes['rate'],elm.attributes['currency'])
          base.add(cur)
        end
        data.add(base)
      end
      data
    end

  end




  #----------------FXData------------------------
  class FXData

    def initialize
      @list =  Array.new
    end


    def add(fx_base_date_obj)
      @list.push(fx_base_date_obj)
    end



    def get_data_by_date(time)
      @list.each do
      |base_date|
        if base_date.get_date == time
          return base_date
        end
      end
    end

    def get_available_currencies
      @list[0].get_available_currencies
    end

    def get_available_dates
      @cur_arr = Array.new
      @list.each do
      |base_date|
        @cur_arr.push( base_date.get_date )
      end
      @cur_arr

    end

    def get_data
      @list
    end

  end

#-------------- FxBaseDate --------------------
  class FXBaseDate

    def initialize(date)
      @date = date
      @list = Array.new
    end


    def add(fx_currency_obj)
      @list.push(fx_currency_obj)
    end

    def get_rate_by_currency(currency)
      found_currency = nil
      @list.each do
      |fx_currency|

        if fx_currency.get_currency == currency
          found_currency = fx_currency
          break
        end

      end
      found_currency
    end

    def get_available_currencies
     @cur_arr = Array.new
      @list.each do
      |fx_currency|
          @cur_arr.push( fx_currency.get_currency )
      end

        @sorted = @cur_arr.sort_by(&:downcase)
    end

    def get_date
      @date
    end

    def set_date=(value)
      @date = value
    end

    def get_currency_data
      @list
    end
  end


  #-------------- FXCurrency ------------------

  class FXCurrency

    def initialize(rate, currency)
      @rate = rate.to_f
      @currency = currency
    end

    def get_rate
      @rate
    end

    def set_rate=(value)
      @rate = value
    end

    def get_currency
      @currency
    end

    def set_currency=(value)
      @currency = value
    end
  end

end

#--------  how to use --------
#ExchangeRate.refresh("./sourceRate.xml")
#puts ExchangeRate.get_available_currencies # to create the array  of possible currency
#puts ExchangeRate.get_available_dates # to create the array of possible dates
#puts ExchangeRate.at('2015-09-02', 'EUR','GBP') # get rate base on date and currency
